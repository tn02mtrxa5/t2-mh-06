#include <stdio.h>
#include <conio.h>
int gcd (int,int);
int main()
{
    int a,b, ans;

    printf("Enter two integers:\n ");
    scanf("%d \n%d", &a, &b);
    ans=gcd(a,b);
    printf("G.C.D of %d and %d is %d", a, b, ans);
    return 0;
}
int gcd (int a, int b)
{
    int ans,i;
    for(i=1; i <= a && i <= b; ++i)
    {
        // Checks if i is factor of both integers
        if(a%i==0 && b%i==0)
            ans = i;
    }
    return ans;
}
